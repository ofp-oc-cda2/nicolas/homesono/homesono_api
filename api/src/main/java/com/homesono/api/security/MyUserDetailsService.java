package com.homesono.api.security;


import com.homesono.api.controller.CategoriesController;
import com.homesono.api.exception.APiRequestException;
import com.homesono.api.model.User;
import com.homesono.api.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Security service responsible to retrieve user details from their email
 */
@Service
public class MyUserDetailsService implements UserDetailsService {

    protected final static Logger logger = LoggerFactory.getLogger(CategoriesController.class);


    @Autowired
    private UserRepository userRepository;

    /**
     * Load a user from DB using its email address, and create a Spring UserDetails with the retrieved information
     *
     * @param param A user email
     * @return An instance of Spring UserDetails
     * @throws APiRequestException when no users were found
     */
    @Override
    public UserDetails loadUserByUsername(String param) throws UsernameNotFoundException {
        User result = userRepository.findUserById(Integer.valueOf(param));

        if (result == null) {
            throw new APiRequestException(HttpStatus.CONFLICT, "Il n'a ete trouve aucun utilisateur, ni par le username, ,ni par l'e-mail.");
        }

        if ("admin".equals(result.getUsername())) {
            logger.info("ROLE_ADMIN");
            return org.springframework.security.core.userdetails.User
                    .withUsername(result.getUsername())
                    .password(result.getPassword())
                    .authorities("ROLE_ADMIN")
                    .accountExpired(false)
                    .accountLocked(false)
                    .credentialsExpired(false)
                    .disabled(false)
                    .build();
        }

        logger.info("ROLE_USER");
        return org.springframework.security.core.userdetails.User
                .withUsername(result.getUsername())
                .password(result.getPassword())
                .authorities("ROLE_USER")
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();
    }
}
