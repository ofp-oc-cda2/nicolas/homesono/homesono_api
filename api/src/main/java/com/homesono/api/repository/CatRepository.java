package com.homesono.api.repository;

import com.homesono.api.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CatRepository extends JpaRepository<Category, Integer> {

    @Query(value = "SELECT c FROM Category c WHERE c.catIdCatPK=:catIdCatPK")
    Category findCategoryById(@Param("catIdCatPK") Integer catIdCatPK);

    @Override
    void deleteById(Integer id);


}