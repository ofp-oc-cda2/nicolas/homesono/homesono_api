package com.homesono.api.repository;

import com.homesono.api.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    @Query(value = "SELECT p FROM Product p WHERE p.proIdProPK=:proIdProPK")
    Product findProductById(Integer proIdProPK);

    @Override
    void deleteById(Integer id);
}
