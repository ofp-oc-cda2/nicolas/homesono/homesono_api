package com.homesono.api.repository;

import com.homesono.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "SELECT u FROM User u WHERE u.username=:param OR u.email=:param")
    User findUserByUsernameOrEmail(@Param("param") String param);

    @Query(value = "SELECT u FROM User u WHERE u.id=:id")
    User findUserById(@Param("id") Integer id);

    @Query(value = "SELECT u FROM User u WHERE u.username NOT LIKE 'admin'")
    List<User> findUserByIdAdminExcluded();

    @Query(value = "SELECT u FROM User u WHERE u.email=:email")
    User findUserByEmail(@Param("email") String email);

    @Query(value = "SELECT u FROM User u WHERE u.email=:email  AND NOT u.id=:id")
    User findUserByEmailCurrentUserExcluded(@Param("email") String email, @Param("id") int id);

    @Query(value = "SELECT u FROM User u WHERE u.username=:username  AND NOT u.id=:id")
    User findUserByUsernameCurrentUserExcluded(@Param("username") String username, @Param("id") int id);

    @Query(value = "SELECT u FROM User u WHERE u.username=:username")
    User findUserByUsername(@Param("username") String username);

    @Override
    void deleteById(Integer id);
}
