package com.homesono.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PRO_ID_PRO_PK")
    private Integer proIdProPK;

    @Column(name = "PRO_NAME_PRO")
    private String proNamePro;

    @Column(name = "PRO_DESC_PRO")
    private String proDescPro;

    @Nullable
    @Column(name = "PRO_PICTURES_PRO")
    private String proPicturesPro;

    @Column(name = "PRO_RATE_PRO")
    private Integer proRatePro;

    @Column(name = "PRO_INSTRUCTIONS_PRO")
    private String proInstructionsPro;

    @Column(name = "PRO_BOOKING_BKG_FK")
    private Integer proBookingBkgFK;


    @JoinColumn(name = "PRO_CATEGORY_CAT_FK", nullable = true)
    @ManyToOne
    @JsonIgnoreProperties({"productCollection"})
    private Category proCategoryCatFk;

    public Product() {

    }

    public Integer getProIdProPK() {
        return proIdProPK;
    }

    public void setProIdProPK(Integer proIdProPK) {
        this.proIdProPK = proIdProPK;
    }

    public String getProNamePro() {
        return proNamePro;
    }

    public void setProNamePro(String proNamePro) {
        this.proNamePro = proNamePro;
    }

    public String getProDescPro() {
        return proDescPro;
    }

    public void setProDescPro(String proDescPro) {
        this.proDescPro = proDescPro;
    }

    public String getProPicturesPro() {
        return proPicturesPro;
    }

    public void setProPicturesPro(String proPicturesPro) {
        this.proPicturesPro = proPicturesPro;
    }

    public Integer getProRatePro() {
        return proRatePro;
    }

    public void setProRatePro(Integer proRatePro) {
        this.proRatePro = proRatePro;
    }

    public String getProInstructionsPro() {
        return proInstructionsPro;
    }

    public void setProInstructionsPro(String proInstructionsPro) {
        this.proInstructionsPro = proInstructionsPro;
    }

    public Integer getProBookingBkgFK() {
        return proBookingBkgFK;
    }

    public void setProBookingBkgFK(Integer proBookingBkgFK) {
        this.proBookingBkgFK = proBookingBkgFK;
    }

    public Category getProCategoryCatFk() {
        return proCategoryCatFk;
    }

    public void setProCategoryCatFk(Category proCategoryCatFk) {
        this.proCategoryCatFk = proCategoryCatFk;
    }
}
