package com.homesono.api.model;

import javax.persistence.*;

@Entity
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "REV_ID_REV_PK")
    private Integer revIdRevPK;
    @Column(name = "REV_IDBOOKING_BKG_FK")
    private Integer revIdbookingBkgFK;
    @Column(name = "REV_CONTENT_REV")
    private Integer revContentRev;
    @Column(name = "REV_MARK_REV")
    private Integer revMarkRev;
    @Column(name = "REV_VERIFIED_REV")
    private Integer revVerifiedRev;

    public Integer getRevIdRevPK() {
        return revIdRevPK;
    }

    public void setRevIdRevPK(Integer revIdRevPK) {
        this.revIdRevPK = revIdRevPK;
    }

    public Integer getRevIdbookingBkgFK() {
        return revIdbookingBkgFK;
    }

    public void setRevIdbookingBkgFK(Integer revIdbookingBkgFK) {
        this.revIdbookingBkgFK = revIdbookingBkgFK;
    }

    public Integer getRevContentRev() {
        return revContentRev;
    }

    public void setRevContentRev(Integer revContentRev) {
        this.revContentRev = revContentRev;
    }

    public Integer getRevMarkRev() {
        return revMarkRev;
    }

    public void setRevMarkRev(Integer revMarkRev) {
        this.revMarkRev = revMarkRev;
    }

    public Integer getRevVerifiedRev() {
        return revVerifiedRev;
    }

    public void setRevVerifiedRev(Integer revVerifiedRev) {
        this.revVerifiedRev = revVerifiedRev;
    }
}
