package com.homesono.api.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "BKG_ID_BKG_PK")
    private Integer bgkIdBgkPK;
    @Column(name = "BKG_IDUSER_USR_FK")
    private Integer bgkIduserUsrFK;
    @Column(name = "BKG_ID_PRODUCT_PRO_FK")
    private Integer bgkIdproductBgk;
    @Column(name = "BKG_PICKUPDATE_BKG")
    private Date bgkPickupdateBgk;
    @Column(name = "BKG_DROPOFFDATE_BKG")
    private Date bgkDropoffdateBgk;
    @Column(name = "BGK_SUM_BKG")
    private Integer bgkSumBgk;
    @Column(name = "BKG_MESSAGE_BKG")
    private String bgkMessageBgk;
    @Column(name = "BKG_RESPONSE_BKG")
    private String bgkResponseBgk;
    @Column(name = "BKG_ISCLIENTREACHED_BKG")
    private Boolean bgkIsclientreachedBgk;
    @Column(name = "BKG_STATUS_BKG")
    private String bgkStatusBgk;
    @Column(name = "BKG_ISPROCESSED_BKG")
    private Boolean bgkIsprocessedBgk;

    public Integer getBgkIdBgkPK() {
        return bgkIdBgkPK;
    }

    public void setBgkIdBgkPK(Integer bgkIdBgkPK) {
        this.bgkIdBgkPK = bgkIdBgkPK;
    }

    public Integer getBgkIduserUsrFK() {
        return bgkIduserUsrFK;
    }

    public void setBgkIduserUsrFK(Integer bgkIduserUsrFK) {
        this.bgkIduserUsrFK = bgkIduserUsrFK;
    }

    public Integer getBgkIdproductBgk() {
        return bgkIdproductBgk;
    }

    public void setBgkIdproductBgk(Integer bgkIdproductBgk) {
        this.bgkIdproductBgk = bgkIdproductBgk;
    }

    public Date getBgkPickupdateBgk() {
        return bgkPickupdateBgk;
    }

    public void setBgkPickupdateBgk(Date bgkPickupdateBgk) {
        this.bgkPickupdateBgk = bgkPickupdateBgk;
    }

    public Date getBgkDropoffdateBgk() {
        return bgkDropoffdateBgk;
    }

    public void setBgkDropoffdateBgk(Date bgkDropoffdateBgk) {
        this.bgkDropoffdateBgk = bgkDropoffdateBgk;
    }

    public Integer getBgkSumBgk() {
        return bgkSumBgk;
    }

    public void setBgkSumBgk(Integer bgkSumBgk) {
        this.bgkSumBgk = bgkSumBgk;
    }

    public String getBgkMessageBgk() {
        return bgkMessageBgk;
    }

    public void setBgkMessageBgk(String bgkMessageBgk) {
        this.bgkMessageBgk = bgkMessageBgk;
    }

    public String getBgkResponseBgk() {
        return bgkResponseBgk;
    }

    public void setBgkResponseBgk(String bgkResponseBgk) {
        this.bgkResponseBgk = bgkResponseBgk;
    }

    public Boolean getBgkIsclientreachedBgk() {
        return bgkIsclientreachedBgk;
    }

    public void setBgkIsclientreachedBgk(Boolean bgkIsclientreachedBgk) {
        this.bgkIsclientreachedBgk = bgkIsclientreachedBgk;
    }

    public String getBgkStatusBgk() {
        return bgkStatusBgk;
    }

    public void setBgkStatusBgk(String bgkStatusBgk) {
        this.bgkStatusBgk = bgkStatusBgk;
    }

    public Boolean getBgkIsprocessedBgk() {
        return bgkIsprocessedBgk;
    }

    public void setBgkIsprocessedBgk(Boolean bgkIsprocessedBgk) {
        this.bgkIsprocessedBgk = bgkIsprocessedBgk;
    }
}
