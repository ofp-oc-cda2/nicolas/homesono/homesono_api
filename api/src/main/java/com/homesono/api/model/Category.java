package com.homesono.api.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.List;

@Entity // This tells Hibernate to make a table out of this class
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CAT_ID_CAT_PK")
    @ApiModelProperty(notes = "category ID", example = "1", required = true)
    private Integer catIdCatPK;
    @Column(name = "CAT_NAME_CAT")
    @ApiModelProperty(notes = "category name", example = "mariage", required = true)
    private String catNameCat;

    @OneToOne
    @ApiModelProperty(notes = "category image", example = "file", required = true)
    private FileDB fileDB;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proCategoryCatFk")
    private List<Product> productCollection;

    public List<Product> getProductCollection() {
        return productCollection;
    }

    public void setProductCollection(List<Product> productCollection) {
        this.productCollection = productCollection;
    }

    public Category() {
    }

    public Category(Integer catIdCatPK, String catNameCat) {
        this.catIdCatPK = catIdCatPK;
        this.catNameCat = catNameCat;
    }

    public Integer getCatIdCatPK() {
        return catIdCatPK;
    }

    public void setCatIdCatPK(Integer catIdCatPK) {
        this.catIdCatPK = catIdCatPK;
    }

    public String getCatNameCat() {
        return catNameCat;
    }

    public void setCatNameCat(String catNameCat) {
        this.catNameCat = catNameCat;
    }

    public FileDB getFileDB() {
        return fileDB;
    }

    public void setFileDB(FileDB fileDB) {
        this.fileDB = fileDB;
    }
}