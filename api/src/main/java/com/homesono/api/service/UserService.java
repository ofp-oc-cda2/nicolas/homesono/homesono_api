package com.homesono.api.service;


import com.homesono.api.controller.UsersController;
import com.homesono.api.exception.APiRequestException;
import com.homesono.api.model.SignInObject;
import com.homesono.api.model.User;
import com.homesono.api.repository.UserRepository;
import com.homesono.api.security.JwtTokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UserService {

    protected final static Logger logger = LoggerFactory.getLogger(UsersController.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public static boolean patternMatches(String emailAddress, String regexPattern) {
        return Pattern.compile(regexPattern)
                .matcher(emailAddress)
                .matches();
    }

    /**
     * @param body
     * @return
     */
    public User signup(SignInObject body) {

        String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        Boolean emailValidation = patternMatches(body.getEmail(), regexPattern);
        logger.info("emailValidation : [" + emailValidation + "]");

        if (!emailValidation) {
            throw new APiRequestException(HttpStatus.BAD_REQUEST, "L'e-mail renseigné n'est pas au bon format.");
        }

        User resultByEmail = userRepository.findUserByEmail(body.getEmail());
        User resultByUsername = userRepository.findUserByUsername(body.getUsername());

        if (resultByEmail != null) {
            throw new APiRequestException(HttpStatus.CONFLICT, "Cet e-mail est déjà utilisé, merci d'en renseigner un autre.");
        }

        if (resultByUsername != null) {
            throw new APiRequestException(HttpStatus.CONFLICT, "Cet identifiant est déjà utilisé, merci d'en choisir un autre.");
        }

        User userToSave = new User(body.getUsername(), passwordEncoder.encode(body.getPassword()), null, null, body.getEmail(), "USER");

        return userRepository.save(userToSave);
    }

    /**
     * @param param
     * @param password
     * @return
     */
    public String login(String param, String password) {
        User result = userRepository.findUserByUsernameOrEmail(param);
        if (result == null) {
            throw new APiRequestException(HttpStatus.BAD_REQUEST, "Cet utilisateur est inconnu");
        }
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(result.getId(), password));

        return jwtTokenProvider.createToken(result);
    }

    /**
     * @param remoteUser
     * @return
     */
    public User whoami(String remoteUser) {
        User userResult = userRepository.findUserByUsername(remoteUser);
        if (userResult == null) {
            throw new APiRequestException(HttpStatus.NOT_FOUND, "No such user");
        }
        return userResult;
    }

    /**
     * @param remoteUser
     * @return
     */
    public String refreshToken(String remoteUser) {
        User user = userRepository.findUserByUsernameOrEmail(remoteUser);
        return jwtTokenProvider.createToken(user);
    }

    /**
     * @param user
     * @return
     */
    public User update(User user) {
        if (user.getPassword() != null)
            user.setPassword(passwordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }

}