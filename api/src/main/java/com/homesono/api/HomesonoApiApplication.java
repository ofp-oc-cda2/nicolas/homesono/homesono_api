package com.homesono.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//adresse du swagger
//http://localhost:8000/swagger
@SpringBootApplication
public class HomesonoApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomesonoApiApplication.class, args);
    }

}
