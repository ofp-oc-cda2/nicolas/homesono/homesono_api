package com.homesono.api.controller;

import com.homesono.api.exception.APiRequestException;
import com.homesono.api.model.Category;
import com.homesono.api.repository.CatRepository;
import com.homesono.api.service.FileStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/")
@Api(value = "Ce controller nous permet d'interagir avec la table Categories dans la base de données", produces = "application/json")
public class CategoriesController {

    protected final static Logger logger = LoggerFactory.getLogger(CategoriesController.class);

    @Autowired
    private CatRepository catRepository;

    @Autowired
    private FileStorageService storageService;

    @RolesAllowed("ROLE_ADMIN")
    @PostMapping(path = "/categories")
    public @ResponseBody Category addNewCategorie(@RequestBody Category body) {
        logger.info("appel a POST/categories");

        Category newCat = new Category();
        newCat.setCatNameCat(body.getCatNameCat());
        return catRepository.save(newCat);
    }

    //    @GetMapping(path = "/categories")
    @ApiOperation(value = "permet d'obtenir la liste de toutes les categories", produces = "application/json", response = Category.class, nickname = "get.all.categories")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "l operation s est deroule correctement", response = Category.class)})
    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public @ResponseBody List<Category> getAllCategories() {
        logger.info("appel a GET/categories");

        return catRepository.findAll();
    }

    //    @GetMapping(path = "/categories/{id}")
    @ApiOperation(value = "permet d'obtenir une categorie par son identifiant", produces = "application/json", response = Category.class, nickname = "get.one.category")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "l operation s est deroule correctement", response = Category.class)})
    @RequestMapping(value = "/categories/{id}", method = RequestMethod.GET)
    public @ResponseBody Category getCategoryById(@PathVariable Integer id) {
        logger.info("appel a GET/categories/" + id);

        return catRepository.findCategoryById(id);
    }

    @RolesAllowed("ROLE_ADMIN")
    @PutMapping(path = "/categories/{id}")
    public @ResponseBody Category updateCategoryById(@PathVariable Integer id, @RequestBody String name) {
        logger.info("appel a PUT/categories/" + id);

        if (!catRepository.existsById(id)) {
            throw new APiRequestException(HttpStatus.NOT_FOUND, "La catégorie " + id + " n'existe pas");
        }

        Category catToProcess = catRepository.findCategoryById(id);
        catToProcess.setCatNameCat(name);
        catRepository.save(catToProcess);
        return catRepository.findCategoryById(id);
    }

    @RolesAllowed("ROLE_ADMIN")
    @DeleteMapping(path = "/categories/{id}")
    public @ResponseBody Category deleteCategoryById(@PathVariable Integer id) {
        logger.info("appel a DELETE/categories/" + id);

        Category deletedCategory = catRepository.findCategoryById(id);
        catRepository.deleteById(id);
        return deletedCategory;
    }

}