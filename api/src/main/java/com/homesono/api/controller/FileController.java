package com.homesono.api.controller;

import com.homesono.api.exception.APiRequestException;
import com.homesono.api.model.Category;
import com.homesono.api.model.FileDB;
import com.homesono.api.repository.CatRepository;
import com.homesono.api.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
//@CrossOrigin("http://localhost:8081")
public class FileController {
    protected final static Logger logger = LoggerFactory.getLogger(CategoriesController.class);

    @Autowired
    private FileStorageService storageService;

    @Autowired
    private CatRepository catRepository;

    @PostMapping("/upload/{id}")
    public ResponseEntity<FileDB> uploadFile(@PathVariable Integer id, @RequestParam("file") MultipartFile file) {
        logger.info("appel a POST/upload");

        String message = "";
        FileDB fileDBToReturn;

        try {
            if (file.getSize() > 1024 * 1024 * 2.5) {
                throw new APiRequestException(HttpStatus.NOT_FOUND, "Fichier trop volumineux");
            }

            fileDBToReturn = storageService.store(file);

            Category catToProcess = catRepository.findCategoryById(id);
            catToProcess.setFileDB(fileDBToReturn);
            catRepository.save(catToProcess);

            return ResponseEntity.status(HttpStatus.OK).body(fileDBToReturn);
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + ", reason :  " + e.getMessage();
            throw new APiRequestException(HttpStatus.EXPECTATION_FAILED, message);
        }
    }

    @GetMapping("/files/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable String id) {
        FileDB fileDB = storageService.getFile(id);
        // return fileDBRepository.findById(id).get();

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDB.getName() + "\"")
                .body(fileDB.getData());
    }

    //    @GetMapping("/files")
//    public ResponseEntity<List<ResponseFile>> getListFiles() {
//        List<ResponseFile> files = storageService.getAllFiles().map(dbFile -> {
//            String fileDownloadUri = ServletUriComponentsBuilder
//                    .fromCurrentContextPath()
//                    .path("/files/")
//                    .path(dbFile.getId())
//                    .toUriString();
//
//            return new ResponseFile(
//                    dbFile.getId(),
//                    dbFile.getName(),
//                    fileDownloadUri,
//                    dbFile.getType(),
//                    dbFile.getData().length);
//        }).collect(Collectors.toList());
//
//        return ResponseEntity.status(HttpStatus.OK).body(files);
//    }
}