package com.homesono.api.controller;

import com.homesono.api.model.LogInObject;
import com.homesono.api.model.SignInObject;
import com.homesono.api.model.User;
import com.homesono.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping(path = "/auth/signup", headers = "Accept=application/json")
    public User signup(@RequestBody SignInObject body) {
        return userService.signup(body);
    }

    @PostMapping(path = "/auth/login", headers = "Accept=application/json")
    public String login(@RequestBody LogInObject body) {
        return userService.login(body.getParam(), body.getPassword());
    }

    @GetMapping("/auth/whoami")
    public User whoami(HttpServletRequest req) {
        return userService.whoami(req.getRemoteUser());
    }

    @GetMapping("/auth/refresh")
    public String refresh(HttpServletRequest req) {
        return userService.refreshToken(req.getRemoteUser());
    }
}
