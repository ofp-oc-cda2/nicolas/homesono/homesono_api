package com.homesono.api.controller;

import com.homesono.api.model.Category;
import com.homesono.api.model.Product;
import com.homesono.api.repository.CatRepository;
import com.homesono.api.repository.ProductRepository;
import com.homesono.api.repository.UserRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Controller
public class ProductsController {
    protected final static Logger logger = LoggerFactory.getLogger(CategoriesController.class);

    @Autowired
    private CatRepository catRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductRepository productRepository;

    @PostMapping(path = "/products")
    public @ResponseBody
    Product addNewProduct(@RequestBody Product body) {
        logger.info("appel a POST/products");

        Product productToReturn = productRepository.save(body);
        return productToReturn;
    }

    @ApiOperation(value = "permet d'obtenir la liste de tous les produits", produces = "application/json", response = Category.class, nickname = "get.all.products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "l operation s est deroule correctement", response = Product.class)})
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public @ResponseBody List<Product> getAllProducts() {
        logger.info("appel a GET/products");

        return productRepository.findAll();
    }

    @GetMapping(path = "/products/{id}")
    public @ResponseBody
    Product getProductById(@PathVariable Integer id) {
        logger.info("appel a GET/products/" + id);
        return productRepository.findProductById(id);
    }

    @PutMapping(path = "/products")
    public @ResponseBody
    List<Product> updateProductById(@RequestBody Product body) {
        logger.info("appel a PUT/products");
        Product productToProcess = body; // this replaces every field of the old user by the body we receive from the front.
        productRepository.save(productToProcess);
        return productRepository.findAll();
    }

    @DeleteMapping(path = "/products/{id}")
    public @ResponseBody
    List<Product> deleteProductById(@PathVariable Integer id) {
        logger.info("appel a DELETE/products/" + id);

        productRepository.deleteById(id);
        return productRepository.findAll();
    }
}