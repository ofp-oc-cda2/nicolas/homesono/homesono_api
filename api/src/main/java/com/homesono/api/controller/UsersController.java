package com.homesono.api.controller;

import com.homesono.api.exception.APiRequestException;
import com.homesono.api.model.SignInObject;
import com.homesono.api.model.User;
import com.homesono.api.repository.CatRepository;
import com.homesono.api.repository.ProductRepository;
import com.homesono.api.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

import static com.homesono.api.service.UserService.patternMatches;

@CrossOrigin(origins = "*")
@RestController
public class UsersController {
    protected final static Logger logger = LoggerFactory.getLogger(UsersController.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CatRepository catRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
            + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

    @GetMapping(path = "/users")
    public @ResponseBody List<User> getAllUsers() {
        logger.info("appel a GET/users");
        return userRepository.findUserByIdAdminExcluded();
    }

    @RolesAllowed("ROLE_ADMIN")
    @PostMapping(path = "/users")
    public @ResponseBody User addNewUser(@RequestBody SignInObject body) {
        logger.info("appel a POST/users");


        Boolean emailValidation = patternMatches(body.getEmail(), regexPattern);
        logger.info("emailValidation : [" + emailValidation + "]");

        //if (!emailValidation) {
        if (Boolean.FALSE.equals(emailValidation)) {
            throw new APiRequestException(HttpStatus.BAD_REQUEST, "L'e-mail renseigné n'est pas au bon format.");
        }

        User resultByEmail = userRepository.findUserByEmail(body.getEmail());
        User resultByUsername = userRepository.findUserByUsername(body.getUsername());

        if (resultByEmail != null) {
            throw new APiRequestException(HttpStatus.CONFLICT, "Cet e-mail est déjà utilisé, merci d'en renseigner un autre.");
        }

        if (resultByUsername != null) {
            throw new APiRequestException(HttpStatus.CONFLICT, "Cet identifiant est déjà utilisé, merci d'en choisir un autre.");
        }

        User userToSave = new User(body.getUsername(), passwordEncoder.encode(body.getPassword()), null, null, body.getEmail(), "USER");

        return userRepository.save(userToSave);
    }

    @GetMapping(path = "/users/{id}")
    public @ResponseBody User getUserById(@PathVariable Integer id) {
        logger.info("appel a GET/users/" + id);

        if (!userRepository.existsById(id)) {
            throw new APiRequestException(HttpStatus.NOT_FOUND, "l'utilisateur " + id + " n'existe pas");
        }
        return userRepository.findUserById(id);
    }

    @RolesAllowed("ROLE_ADMIN")
    @PutMapping(path = "/users/{id}")
    public @ResponseBody User updateUserById(@PathVariable int id, @RequestBody User body) {
        logger.info("appel a PUT/users/" + id);

        if (!userRepository.existsById(id)) {
            throw new APiRequestException(HttpStatus.NOT_FOUND, "l'utilisateur " + id + " n'existe pas");
        }

        Boolean emailValidation = patternMatches(body.getEmail(), regexPattern);
        logger.info("emailValidation : [" + emailValidation + "]");

        if (!emailValidation) {
            throw new APiRequestException(HttpStatus.BAD_REQUEST, "L'e-mail renseigné n'est pas au bon format.");
        }

        User resultByEmail = userRepository.findUserByEmailCurrentUserExcluded(body.getEmail(), id);
        User resultByUsername = userRepository.findUserByUsernameCurrentUserExcluded(body.getUsername(), id);

        if (resultByEmail != null) {
            throw new APiRequestException(HttpStatus.CONFLICT, "Cet e-mail est déjà utilisé, merci d'en renseigner un autre.");
        }

        if (resultByUsername != null) {
            throw new APiRequestException(HttpStatus.CONFLICT, "Cet identifiant est déjà utilisé, merci d'en choisir un autre.");
        }

        User userToProcess = userRepository.findUserById(id);
        userToProcess.setEmail(body.getEmail());
        userToProcess.setUsername(body.getUsername());
        userToProcess.setFirstName(body.getFirstName());
        userToProcess.setLastName(body.getLastName());
//        userToProcess = body; // this replaces every field of the old user by the body we receive from the front.

        userRepository.save(userToProcess);

        return userToProcess;
    }

    @RolesAllowed("ROLE_ADMIN")
    @DeleteMapping(path = "/users/{id}")
    public @ResponseBody User deleteUserById(@PathVariable Integer id) {
        logger.info("appel a DELETE/users/" + id);

        User deletedUser = userRepository.findUserById(id);
        userRepository.deleteById(id);
        return deletedUser;
    }
}