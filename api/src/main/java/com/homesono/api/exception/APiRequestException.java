package com.homesono.api.exception;

import org.springframework.http.HttpStatus;

public class APiRequestException extends RuntimeException {

    private final HttpStatus status;

    public APiRequestException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return this.status;
    }
}
