package com.homesono.api.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler({APiRequestException.class})
    public ResponseEntity<ApiException> handleApiRequestException(APiRequestException ex) {

        String pattern = "EEEEE MMMMM yyyy HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("fr", "FR"));
        String date = simpleDateFormat.format(new Date());

//          Return response entity
        return new ResponseEntity<>(new ApiException(ex.getMessage(), ex.getStatus(), date), ex.getStatus());

    }
}