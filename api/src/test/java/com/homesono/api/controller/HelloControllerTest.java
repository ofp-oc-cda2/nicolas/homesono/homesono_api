package com.homesono.api.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
// import all the static methods from the Assertions class, so we may use them in this class


class HelloControllerTest {
    @Test
    void hello() {
        HelloController controller = new HelloController(); // instance of the controller
        String response = controller.hello("world"); // act
        assertEquals("Hello world", response); // assert
    }
}
