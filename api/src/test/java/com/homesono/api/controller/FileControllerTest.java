package com.homesono.api.controller;

import com.homesono.api.RestTestsConfiguration;
import com.homesono.api.model.Category;
import com.homesono.api.model.FileDB;
import com.homesono.api.repository.CatRepository;
import com.homesono.api.service.FileStorageService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FileController.class)
@Import(RestTestsConfiguration.class)
class FileControllerTest {

    @MockBean
    private FileStorageService storageService;
    //    Fournir un @MockBean pour chaque repository qu'on a @Autowired dans le vrai controller.

    @MockBean
    private CatRepository catRepository;
//    Fournir un @MockBean pour chaque repository qu'on a @Autowired dans le vrai controller.

    @Autowired
    private MockMvc mvc;

    @Test
    void testToUploadABiggerFile() throws Exception {

        byte[] bytes = new byte[(int) Math.round(1024 * 1024 * 2.6)];
//        cette forme de calcul permet de comprendre que c'est 2.5mo de fichier.
//        Math.round() est utilisé ici parce qu'on alloue un tableau, donc on peut pas avoir de moitié de case.
//        Je cast en int, car je sais que mon long ne dépassera pas taille maximale autorisée pour un int.
//        (2147483647).

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "hello.jpg",
                MediaType.TEXT_PLAIN_VALUE,
                bytes
        );

        given(storageService.store(Mockito.any())).willReturn(new FileDB());
        given(catRepository.findCategoryById(1)).willReturn(new Category());
        given(catRepository.save(Mockito.any(Category.class))).willReturn(null);
//        https://stackoverflow.com/questions/21800726/using-spring-mvc-test-to-unit-test-multipart-post-request
        mvc.perform(MockMvcRequestBuilders.multipart("/upload/1")
                        .file(file))
                .andExpect(status().is(417));
//        417 Expectation failed

    }

    @Test
    void testUploadFile() throws Exception {

        byte[] bytes = new byte[(int) Math.round(1024 * 1024 * 2.4)];
//        cette forme de calcul permet de comprendre que c'est 2,4mo de fichier.'
//        Math.round() est utilisé ici parce qu'on alloue un tableau, donc on peut pas avoir de moitié de case.
//        Je cast en int, car je sais que mon long ne dépassera pas taille maximale autorisée pour un int (2 147 483 647).

        MockMultipartFile file = new MockMultipartFile(
                "file",
                "hello.jpg",
                MediaType.TEXT_PLAIN_VALUE,
                bytes
        );

//        https://stackoverflow.com/questions/21800726/using-spring-mvc-test-to-unit-test-multipart-post-request

        given(storageService.store(Mockito.any())).willReturn(new FileDB("file", "jpeg", new byte[0]));
        given(catRepository.findCategoryById(1)).willReturn(new Category());
        given(catRepository.save(Mockito.any(Category.class))).willReturn(null);
        mvc.perform(MockMvcRequestBuilders.multipart("/upload/1")
                        .file(file))
                .andExpect(status().is(200));
    }
}