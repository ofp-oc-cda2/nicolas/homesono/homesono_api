package com.homesono.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.homesono.api.RestTestsConfiguration;
import com.homesono.api.model.SignInObject;
import com.homesono.api.model.User;
import com.homesono.api.repository.CatRepository;
import com.homesono.api.repository.ProductRepository;
import com.homesono.api.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(UsersController.class)
@Import(RestTestsConfiguration.class)
class UsersControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private CatRepository catRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ProductRepository productRepository;

    @Test
    @WithMockUser(username = "admin", roles = {"ADMIN"})
    void testAddNewUser() throws Exception {

//        voila ce qui arrive du formulaire
        SignInObject testSignInObject = new SignInObject();
        testSignInObject.setUsername("nicolas");
        testSignInObject.setEmail("contact@nicolasmaes.fr");
        testSignInObject.setPassword("azerty");

//        voilà ce que j'espere en sortie de fonction
        User testUser = new User(
                1000,
                "nicolas",
                "contact@nicolasmaes.fr",
                "azerty");

//        ces lignes implémentent les méthodes présentes dans la fonction que l'on teste sur le mock, en déterminant le retour selon ce qu'on lui donne, on simule un scénario complet ici.
        given(userRepository.findUserByEmail(testSignInObject.getEmail())).willReturn(null);
        given(userRepository.findUserByUsername(testSignInObject.getUsername())).willReturn(null);
        given(userRepository.save(Mockito.any(User.class))).willReturn(new User(1000, testSignInObject.getUsername(), testSignInObject.getPassword(), testSignInObject.getEmail()));

        mvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testSignInObject)))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id", is(testUser.getId())))
                .andExpect(jsonPath("$.username", is(testUser.getUsername())));
    }
}