package com.homesono.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.homesono.api.RestTestsConfiguration;
import com.homesono.api.model.Category;
import com.homesono.api.repository.CatRepository;
import com.homesono.api.service.FileStorageService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CategoriesController.class)
@Import(RestTestsConfiguration.class)
class CategoriesControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CatRepository catRepository;
//    Fournir un @MockBean pour chaque repository
//    qu'on a @Autowired dans le vrai controller.

    @MockBean
    private FileStorageService storageService;
//    Fournir un @MockBean pour chaque repository
//    qu'on a @Autowired dans le vrai controller.

    @Test
    void testAddNewCategorie() throws Exception {
        Category testCategory = new Category();
        testCategory.setCatNameCat("nameTest");

        given(catRepository.save(Mockito.any(Category.class))).willReturn(testCategory);
//        cette ligne implémente la méthode save sur le mock, en déterminant le retour
//        selon ce qu'on lui donne

        mvc.perform(post("/categories")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(testCategory)))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.catNameCat", is(testCategory.getCatNameCat())));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
    void updateCategoryById() throws Exception {
        Category testCategory = new Category();
        testCategory.setCatNameCat("stanley");
        testCategory.setCatIdCatPK(1);

        //        ces lignes implémentent les méthodes présentes dans la fonction que l'on teste sur le mock, en déterminant le retour selon ce qu'on lui donne, on simule un scénario complet ici.
        given(catRepository.existsById(testCategory.getCatIdCatPK())).willReturn(true);
        given(catRepository.findCategoryById(testCategory.getCatIdCatPK())).willReturn(testCategory);

        given(catRepository.save(Mockito.any(Category.class))).willReturn(testCategory);
//        cette ligne implémente la méthode save sur le mock, en déterminant le retour selon ce qu'on lui donne

        mvc.perform(put("/categories/" + testCategory.getCatIdCatPK())
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content("newCategoryName"))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.catIdCatPK", is(testCategory.getCatIdCatPK())))
                .andExpect(jsonPath("$.catNameCat", is("newCategoryName")));
    }

    @Test
    void testGetAllCategories() throws Exception {
//        Nous verifions ici que cette méthode renvoie bien une liste.
        List<Category> testList = new ArrayList<Category>();

        Category category1 = new Category(1, "son");
        Category category2 = new Category(2, "lumiere");
        testList.add(category1);
        testList.add(category2);

        given(catRepository.findAll()).willReturn(testList);

        mvc.perform(get("/categories"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].catNameCat", is("son")))
                .andExpect(jsonPath("$[1].catNameCat", is("lumiere")));

//        https://mkyong.com/spring-boot/spring-test-how-to-test-a-json-array-in-jsonpath/
    }
}