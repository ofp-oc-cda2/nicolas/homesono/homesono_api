package com.homesono.api.controller;

import com.homesono.api.model.User;
import com.homesono.api.repository.UserRepository;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = UsersController.class)
@AutoConfigureMockMvc
@DataJpaTest
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class UsersControllerTest_v1 {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository repository;

    // Default test instance
    private final User testUser = new User(1, "nico", "nicolas.maes1@gmail.com", "password");

    @Test
    @WithAnonymousUser
    void addNewUser() {
        repository.save(testUser);
        Iterable<User> users = repository.findAll();
        assertEquals(1, IterableUtil.sizeOf(users));
        assertEquals("id", users.iterator().next().getId());
    }
}