package com.homesono.api;

import com.homesono.api.security.JwtTokenProvider;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/**
 * Extended configuration for REST tests injecting a dummy instance of UserDetailsService in context
 * Nous injectons ici les Bean à utiliser dans les tests, au cas où le test nous dit qu'il ne les trouve pas.
 */
@TestConfiguration
public class RestTestsConfiguration {

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        return new InMemoryUserDetailsManager();
    }

    @Bean
    @Primary
    public JwtTokenProvider jwtTokenProvider() {
        return new JwtTokenProvider();
    }

}
